import React, { useState, useEffect } from 'react'
import {
  Alert,
  Button,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Jumbotron,
  Card,
  CardBody
} from 'reactstrap'
import toast from 'toasted-notes'

import { login } from './Api'
import { saveToken, getToken, saveUser } from '../Utils'

function LoginPage(props) {
  const { history } = props

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const handleChangeEmail = e => setEmail(e.target.value)
  const handleChangePassword = e => setPassword(e.target.value)
  const handleLogin = e => {
    e.preventDefault()

    const payload = { email, password }
    login(payload)
      .then(response => {
        const token = response.data.api_token
        const level = response.data.level
        saveUser(level)
        saveToken(token)
        history.push('/')
      })
      .catch(error => {
        const message = error.response
          ? error.response.data.message
          : 'Login gagal'

        toast.notify(({ onClose }) => (
          <Alert color="danger" toggle={onClose}>
            {message}
          </Alert>
        ))
      })
  }

  useEffect(() => {
    const token = getToken()
    if (token) {
      history.push('/')
    }
  })

  return (
    <Container>
      <Row>
        <Col
          sm={{
            size: 6,
            offset: 3
          }}
        >
          <Jumbotron className="mt-5">
            <Card>
              <CardBody>
                <h2 className="mb-3 text-center">Login<hr/></h2>
                <Form onSubmit={handleLogin}>
                  <FormGroup>
                    <Label>Email</Label>
                    <Input
                      value={email}
                      onChange={handleChangeEmail}
                      type="email"
                      required
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label>Password</Label>
                    <Input
                      value={password}
                      onChange={handleChangePassword}
                      type="password"
                      required
                    />
                  </FormGroup>
                  <Button color="primary">Submit</Button>
                </Form>
              </CardBody>
            </Card>
          </Jumbotron>
        </Col>
      </Row>
    </Container>
  )
}

export default LoginPage
