import React, { useState, useEffect } from 'react'
import { Alert, Button, Form, FormGroup, Label, Input, Card, CardHeader, CardBody } from 'reactstrap'
import { getSupplier, deleteSupplier, putSupplier } from './Api'
import { getToken, handleError } from '../Utils'
import toast from 'toasted-notes'

function EditPage(props) {
  const { match, history } = props
  const { supplierId } = match.params

  const [loading, setLoading] = useState(false)

  const [nama, setNama] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [alamat, setAlamat] = useState('')

  useEffect(() => {
    setLoading(true)

    const token = getToken()
    getSupplier(token, supplierId)
      .then(response => {
        const data = response.data
        setNama(data.nama)
        setEmail(data.email)
        setPhone(data.phone)
        setAlamat(data.alamat)
        setLoading(false)
      })
      .catch(error => {
        setLoading(false)
        handleError(error)
      })
  }, [supplierId])

  const handleDelete = () => {
    if (loading) return

    setLoading(true)

    const token = getToken()
    setLoading(true)
    deleteSupplier(token, supplierId)
      .then(response => {
        toast.notify(({ onClose }) => (
          <Alert color="info" toggle={onClose}>
            Berhasil hapus data
          </Alert>
        ))
        setLoading(false)
        history.push('/suppliers')
      })
      .catch(error => {
        setLoading(false)
        handleError(error)
      })
  }

  const handleSubmit = e => {
    if (loading) return

    e.preventDefault()

    setLoading(true)

    const token = getToken()
    const payload = {
      nama,
      email,
      phone,
      alamat
    }
    putSupplier(token, supplierId, payload)
      .then(response => {
        const { history } = props
        toast.notify(({ onClose }) => (
          <Alert color="success" toggle={onClose}>
            Berhasil update data
          </Alert>
        ))

        setLoading(false)
        history.push('/suppliers')
      })
      .catch(error => {
        setLoading(false)
        handleError(error)
      })
  }

  return (
    <Card>
      <CardHeader>Edit Supplier</CardHeader>
      <CardBody>
        <div>
          <Form disabled={loading} onSubmit={handleSubmit}>
            <FormGroup>
              <Label>Nama</Label>
              <Input
                value={nama}
                onChange={e => setNama(e.target.value)}
                required
              />
            </FormGroup>
            <FormGroup>
              <Label>Alamat</Label>
              <Input
                value={alamat}
                onChange={e => setAlamat(e.target.value)}
                type="textarea"
                required
              />
            </FormGroup>
            <FormGroup>
              <Label>Email</Label>
              <Input
                value={email}
                onChange={e => setEmail(e.target.value)}
                type="email"
                required
              />
            </FormGroup>
            <FormGroup>
              <Label>Telepon</Label>
              <Input
                value={phone}
                onChange={e => setPhone(e.target.value)}
                required
              />
            </FormGroup>
            <Button color="primary">Simpan</Button>
            <Button
              onClick={handleDelete}
              color="link"
              type="button"
              className="float-right text-danger"
            >
              Hapus
            </Button>
          </Form>
        </div>
      </CardBody>
    </Card>
  )
}

export default EditPage
