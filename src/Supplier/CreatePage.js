import React, { useState } from 'react'
import { Alert, Button, Form, FormGroup, Label, Input, Card, CardHeader, CardBody } from 'reactstrap'
import { postSupplier } from './Api'
import { getToken, handleError } from '../Utils'
import toast from 'toasted-notes'

function CreatePage(props) {
  const [nama, setNama] = useState('')
  const [alamat, setAlamat] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')

  const handleSubmit = e => {
    e.preventDefault()

    const token = getToken()
    const payload = {
      nama,
      alamat,
      email,
      phone
    }
    postSupplier(token, payload)
      .then(response => {
        const { history } = props
        toast.notify(({ onClose }) => (
          <Alert color="success" toggle={onClose}>
            Berhasil tambah data
          </Alert>
        ))
        history.push('/suppliers')
      })
      .catch(error => {
        handleError(error)
      })
  }

  return (
    <Card>
      <CardHeader>Supplier</CardHeader>
      <CardBody>
        <div>
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label>Nama</Label>
              <Input
                value={nama}
                onChange={e => setNama(e.target.value)}
                required
              />
            </FormGroup>
            <FormGroup>
              <Label>Alamat</Label>
              <Input
                value={alamat}
                onChange={e => setAlamat(e.target.value)}
                type="textarea"
                required
              />
            </FormGroup>
            <FormGroup>
              <Label>Email</Label>
              <Input
                value={email}
                onChange={e => setEmail(e.target.value)}
                type="email"
                required
              />
            </FormGroup>
            <FormGroup>
              <Label>Telepon</Label>
              <Input
                value={phone}
                onChange={e => setPhone(e.target.value)}
                required
              />
            </FormGroup>
            <Button color="primary">Simpan</Button>
          </Form>
        </div>
      </CardBody>
    </Card>
  )
}

export default CreatePage
