import React, { useEffect, useState } from 'react'
import { Alert, Button, Table, Card, CardHeader, CardBody } from 'reactstrap'
import { Link } from 'react-router-dom'
import PaginationComponent from 'react-reactstrap-pagination'
import { getToken, getUser } from '../Utils'
import { getSuppliers } from './Api'
import toast from 'toasted-notes'

function ListPage(props) {
  const paginationState = {
    data: [],
    total: 0,
    page: 1,
    perPage: 10
  }
  const [pagination, setPagination] = useState(paginationState)

  const fetchSuppliers = page => {
    const token = getToken()
    getSuppliers(token, page)
      .then(response => {
        const { data, current_page, total, per_page } = response.data
        setPagination({
          data,
          total,
          page: current_page,
          perPage: per_page
        })
      })
      .catch(error => {
        const message = error.response
          ? error.response.data.message
          : 'Terjadi kesalahan, silahkan coba lagi'

        toast.notify(({ onClose }) => (
          <Alert color="danger" toggle={onClose}>
            {message}
          </Alert>
        ))
      })
  }
  const changePage = page => {
    fetchSuppliers(page)
  }

  useEffect(() => {
    const { history } = props
    const level = getUser()
    if(level == 1) {
      history.push('/')
    }
    fetchSuppliers(1)
  }, [])

  const gotoItem = item => {
    const { history } = props
    history.push(`/suppliers/${item.id}`)
  }

  return (
    <Card>
      <CardHeader>Supplier</CardHeader>
      <CardBody>
        <div>
          <Button
            tag={Link}
            to="/suppliers/create"
            color="primary"
            className="mb-3"
          >
            Tambah
          </Button>

          <div className="table-responsive">
            <Table hover className="mb-3">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Email</th>
                  <th>Telepon</th>
                </tr>
              </thead>
              <tbody>
                {pagination.data.map(supplier => (
                  <tr onClick={() => gotoItem(supplier)}>
                    <td>{supplier.id}</td>
                    <td>{supplier.nama}</td>
                    <td>{supplier.alamat}</td>
                    <td>{supplier.email}</td>
                    <td>{supplier.phone}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>

          {pagination.total > pagination.perPage ? (
            <PaginationComponent
              totalItems={pagination.total}
              pageSize={pagination.perPage}
              activePage={pagination.page}
              onSelect={changePage}
            />
          ) : null}
        </div>
      </CardBody>
    </Card>
  )
}

export default ListPage
