import React from 'react'
import classNames from 'classnames'
import { Container } from 'reactstrap'
import NavBar from './Navbar'
import { Switch, Route } from 'react-router-dom'

import Supplier from '../Supplier/ListPage'
import CreateSupplier from '../Supplier/CreatePage'
import EditSupplier from '../Supplier/EditPage'
import Customer from '../Customer/ListPage'
import CreateCustomer from '../Customer/CreatePage'
import EditCustomer from '../Customer/EditPage'
import Employee from '../Employee/ListPage'
import CreateEmployee from '../Employee/CreatePage'
import EditEmployee from '../Employee/EditPage'
import Dashboard from '../Dashboard/Dashboard'

export default props => (
  <Container
    fluid
    className={classNames('content', { 'is-open': props.isOpen }, 'mb-3')}
  >
    <NavBar toggle={props.toggle} />
    <Switch>
      <Route exact path="/" component={Dashboard} />
      <Route exact path="/suppliers/create" component={CreateSupplier} />
      <Route path="/suppliers/:supplierId" component={EditSupplier} />
      <Route exact path="/suppliers" component={Supplier} />
      <Route exact path="/customers/create" component={CreateCustomer} />
      <Route path="/customers/:customerId" component={EditCustomer} />
      <Route exact path="/customers" component={Customer} />
      <Route exact path="/employees/create" component={CreateEmployee} />
      <Route path="/employees/:employeeId" component={EditEmployee} />
      <Route exact path="/employees" component={Employee} />
      <Route exact path="/faq" component={() => 'FAQ'} />
      <Route exact path="/contact" component={() => 'Contact'} />
      <Route exact path="/Home-1" component={() => 'Home-1'} />
      <Route exact path="/Home-2" component={() => 'Home-2'} />
      <Route exact path="/Home-3" component={() => 'Home-3'} />
      <Route exact path="/Page-1" component={() => 'Page-1'} />
      <Route exact path="/Page-2" component={() => 'Page-2'} />
      <Route exact path="/page-1" component={() => 'page-1'} />
      <Route exact path="/page-2" component={() => 'page-2'} />
      <Route exact path="/page-3" component={() => 'page-3'} />
      <Route exact path="/page-4" component={() => 'page-4'} />
    </Switch>
  </Container>
)