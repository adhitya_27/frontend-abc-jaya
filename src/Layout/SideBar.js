import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faHome,
  faBriefcase,
  faPaperPlane,
  faQuestion,
  faImage,
  faCopy,
  faSignOutAlt
} from '@fortawesome/free-solid-svg-icons'
import SubMenu from './SubMenu'
import { NavItem, NavLink, Nav } from 'reactstrap'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import { logout } from '../Auth/Api'
import { clearToken, getUser } from '../Utils'

const SideBar = props => (
  <div className={classNames('sidebar', { 'is-open': props.isOpen })}>
    <div className="sidebar-header">
      <span color="info" onClick={props.toggle} style={{ color: '#fff' }}>
        &times;
      </span>
      <h3>ABC Jaya Abadi</h3>
    </div>
    <div className="side-menu">
      <Nav vertical className="list-unstyled pb-3">
<p className="text-center">Selamat Datang, {getUser() == 1 ? 'Admin' : 'Manager'}</p>
        <NavItem>
          <NavLink onClick={props.onClose} tag={Link} to={'/'}>
            <FontAwesomeIcon icon={faHome} className="mr-2" />
            Home
          </NavLink>
        </NavItem>
        <SubMenu
          onClick={props.onClose}
          title="Pages"
          icon={faCopy}
          items={submenus[1]}
        />
        {getUser()==1 ? null : (
        <NavItem>
          <NavLink onClick={props.onClose} tag={Link} to={'/suppliers'}>
            <FontAwesomeIcon icon={faImage} className="mr-2" />
            Supplier
          </NavLink>
        </NavItem>)}
        <NavItem>
          <NavLink onClick={props.onClose} tag={Link} to={'/customers'}>
            <FontAwesomeIcon icon={faImage} className="mr-2" />
            Customer
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink onClick={props.onClose} tag={Link} to={'/employees'}>
            <FontAwesomeIcon icon={faImage} className="mr-2" />
            Karyawan
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={Link} to={'/faq'}>
            <FontAwesomeIcon icon={faQuestion} className="mr-2" />
            FAQ
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={Link} to={'/contact'}>
            <FontAwesomeIcon icon={faPaperPlane} className="mr-2" />
            Contact
          </NavLink>
        </NavItem>
        <NavItem className="text-danger text-right" sm="7">
          <NavLink tag="a" href="#" onClick={props.onLogout}>
            Logout &nbsp;
            <FontAwesomeIcon icon={faSignOutAlt} className="mr-2" />
          </NavLink>
        </NavItem>
      </Nav>
    </div>
  </div>
)

const submenus = [
  [
    {
      title: 'Home 1',
      target: 'Home-1'
    },
    {
      title: 'Home 2',
      target: 'Home-2'
    },
    {
      itle: 'Home 3',
      target: 'Home-3'
    }
  ],
  [
    {
      title: 'Page 1',
      target: 'Page-1'
    },
    {
      title: 'Page 2',
      target: 'Page-2'
    }
  ]
]

export default SideBar
