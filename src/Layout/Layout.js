import React, { useState } from 'react'
import { clearToken, getToken, clearUser } from '../Utils'
import {
  Container,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  Collapse,
  NavbarToggler
} from 'reactstrap'
import { Link, BrowserRouter as Router } from 'react-router-dom'
import { logout } from '../Auth/Api'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import SideBar from './SideBar'
import Content from './Content'

function Layout(props) {
  const { children, history } = props
  const [isOpen, setIsOpen] = useState(false)

  const toggle = () => setIsOpen(!isOpen)
  const handleLogout = () => {
    const token = getToken()

    clearToken()
    clearUser()
    logout(token).finally(() => {
      history.push('/login')
    })
  }

  return (
    <Router>
      <div className="App wrapper">
        <SideBar toggle={toggle} isOpen={isOpen} onClose={toggle} onLogout={handleLogout}/>
        <Content toggle={toggle} isOpen={isOpen}/>
      </div>
    </Router>
    // <Container fluid>
    //   <Row className="mt-3">
    //     <Col md={3} className="mb-3">
    //         <ListGroup>
    //           <ListGroupItem tag={Link} to="/" action>
    //             Dashboard
    //           </ListGroupItem>
    //           <ListGroupItem tag={Link} to="/sales" action>
    //             Penjualan
    //           </ListGroupItem>
    //           <ListGroupItem tag={Link} to="/purchases" action>
    //             Pembelian
    //           </ListGroupItem>
    //           <ListGroupItem tag={Link} to="/products" action>
    //             Produk
    //           </ListGroupItem>
    //           <ListGroupItem tag={Link} to="/categories" action>
    //             Kategori
    //           </ListGroupItem>
    //           <ListGroupItem tag={Link} to="/suppliers" action>
    //             Suplier
    //           </ListGroupItem>
    //           <ListGroupItem
    //             tag="a"
    //             href="#"
    //             onClick={handleLogout}
    //             className="text-danger"
    //             action
    //           >
    //             Logout
    //           </ListGroupItem>
    //         </ListGroup>
    //     </Col>

    //     <Col
    //       style={{
    //         minHeight: 500,
    //         paddingBottom: 100
    //       }}
    //     >
    //       {children}
    //     </Col>
    //   </Row>
    // </Container>
  )
}

export default Layout
