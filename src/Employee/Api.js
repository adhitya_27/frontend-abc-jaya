import axios from 'axios'

const baseUrl = process.env.REACT_APP_BASE_API_URL

export function getEmployee(token, employeeId) {
  return axios({
    method: 'GET',
    url: `${baseUrl}/employees/${employeeId}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    }
  })
}

export function getEmployees(token, page = 1) {
  return axios({
    method: 'GET',
    url: `${baseUrl}/employees?page=${page}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    }
  })
}

export function postEmployee(token, payload) {
  const { nama, alamat, email, phone } = payload
  return axios({
    method: 'POST',
    url: `${baseUrl}/employees`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    },
    data: {
      nama,
      alamat,
      email,
      phone
    }
  })
}

export function putEmployee(token, employeeId, payload) {
  const { nama, email, phone, alamat } = payload
  return axios({
    method: 'PUT',
    url: `${baseUrl}/employees/${employeeId}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    },
    data: {
      nama,
      email,
      phone,
      alamat
    }
  })
}

export function deleteEmployee(token, employeeId) {
  return axios({
    method: 'DELETE',
    url: `${baseUrl}/employees/${employeeId}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    }
  })
}