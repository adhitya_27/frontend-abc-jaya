import axios from 'axios'

const baseUrl = process.env.REACT_APP_BASE_API_URL

export function getCustomer(token, customerId) {
  return axios({
    method: 'GET',
    url: `${baseUrl}/customers/${customerId}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    }
  })
}

export function getCustomers(token, page = 1) {
  return axios({
    method: 'GET',
    url: `${baseUrl}/customers?page=${page}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    }
  })
}
export function postCustomer(token, payload) {
  const { company_name, alamat, email, phone, contact_person } = payload
  return axios({
    method: 'POST',
    url: `${baseUrl}/customers`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    },
    data: {
      company_name,
      alamat,
      email,
      phone,
      contact_person
    }
  })
}

export function putCustomer(token, customerId, payload) {
  const { company_name, alamat, email, phone, contact_person } = payload
  return axios({
    method: 'PUT',
    url: `${baseUrl}/customers/${customerId}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    },
    data: {
      company_name,
      alamat,
      email,
      phone,
      contact_person
    }
  })
}

export function deleteCustomer(token, customerId) {
  return axios({
    method: 'DELETE',
    url: `${baseUrl}/customers/${customerId}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    }
  })
}