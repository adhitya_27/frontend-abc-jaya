import React, { useState } from 'react'
import { Alert, Button, Form, FormGroup, Label, Input, Card, CardHeader, CardBody } from 'reactstrap'
import { postCustomer } from './Api'
import { getToken, handleError } from '../Utils'
import toast from 'toasted-notes'

function CreatePage(props) {
  const [company_name, setNamaPerusahaan] = useState('')
  const [alamat, setAlamat] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [contact_person, setKontak] = useState('')

  const handleSubmit = e => {
    e.preventDefault()

    const token = getToken()
    const payload = {
      company_name,
      alamat,
      email,
      phone,
      contact_person
    }
    postCustomer(token, payload)
      .then(response => {
        const { history } = props
        toast.notify(({ onClose }) => (
          <Alert color="success" toggle={onClose}>
            Berhasil tambah data
          </Alert>
        ))
        history.push('/customers')
      })
      .catch(error => {
        handleError(error)
      })
  }

  return (
    <Card>
      <CardHeader>Customer</CardHeader>
      <CardBody>
        <div>
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label>Nama Perusahaan</Label>
              <Input
                value={company_name}
                onChange={e => setNamaPerusahaan(e.target.value)}
                required
              />
            </FormGroup>
            <FormGroup>
              <Label>Alamat</Label>
              <Input
                value={alamat}
                onChange={e => setAlamat(e.target.value)}
                type="textarea"
                required
              />
            </FormGroup>
            <FormGroup>
              <Label>Email</Label>
              <Input
                value={email}
                onChange={e => setEmail(e.target.value)}
                type="email"
                required
              />
            </FormGroup>
            <FormGroup>
              <Label>Telepon</Label>
              <Input
                value={phone}
                onChange={e => setPhone(e.target.value)}
                required
              />
            </FormGroup>
            <FormGroup>
                <Label>Kontak</Label>
                <Input
                    value={contact_person}
                    onChange={e => setKontak(e.target.value)}
                    required
              />
            </FormGroup>
            <Button color="primary">Simpan</Button>
          </Form>
        </div>
      </CardBody>
    </Card>
  )
}

export default CreatePage
