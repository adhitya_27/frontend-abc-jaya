import React, { useEffect, useState } from 'react'
import { Alert, Button, Table, Card, CardHeader, CardBody } from 'reactstrap'
import { Link } from 'react-router-dom'
import PaginationComponent from 'react-reactstrap-pagination'
import { getToken } from '../Utils'
import { getCustomers } from './Api'
import toast from 'toasted-notes'

function ListPage(props) {
  const paginationState = {
    data: [],
    total: 0,
    page: 1,
    perPage: 10
  }
  const [pagination, setPagination] = useState(paginationState)

  const fetchCustomers = page => {
    const token = getToken()
    getCustomers(token, page)
      .then(response => {
        const { data, current_page, total, per_page } = response.data
        setPagination({
          data,
          total,
          page: current_page,
          perPage: per_page
        })
      })
      .catch(error => {
        const message = error.response
          ? error.response.data.message
          : 'Terjadi kesalahan, silahkan coba lagi'

        toast.notify(({ onClose }) => (
          <Alert color="danger" toggle={onClose}>
            {message}
          </Alert>
        ))
      })
  }
  const changePage = page => {
    fetchCustomers(page)
  }

  useEffect(() => {
    fetchCustomers(1)
  }, [])

  const gotoItem = item => {
    const { history } = props
    history.push(`/customers/${item.id}`)
  }

  return (
    <Card>
      <CardHeader>Customers</CardHeader>
      <CardBody>
        <div>
          <Button
            tag={Link}
            to="/customers/create"
            color="primary"
            className="mb-3"
          >
            Tambah
          </Button>

          <div className="table-responsive">
            <Table hover className="mb-3">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama Perusahaan</th>
                  <th>Alamat</th>
                  <th>Email</th>
                  <th>Telepon</th>
                  <th>Contact Person</th>
                </tr>
              </thead>
              <tbody>
                {pagination.data.map(customer => (
                  <tr onClick={() => gotoItem(customer)}>
                    <td>{customer.id}</td>
                    <td>{customer.company_name}</td>
                    <td>{customer.alamat}</td>
                    <td>{customer.email}</td>
                    <td>{customer.phone}</td>
                    <td>{customer.contact_person}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>

          {pagination.total > pagination.perPage ? (
            <PaginationComponent
              totalItems={pagination.total}
              pageSize={pagination.perPage}
              activePage={pagination.page}
              onSelect={changePage}
            />
          ) : null}
        </div>
      </CardBody>
    </Card>
  )
}

export default ListPage
