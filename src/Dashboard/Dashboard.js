import React, { useEffect } from 'react'
import { getToken } from '../Utils'

function Dashboard(props) {
  const { history } = props
  useEffect(() => {
    const token = getToken()
    if (!token) {
      history.push('/login')
    }
  })

  return <div>Dashboard</div>
}

export default Dashboard
