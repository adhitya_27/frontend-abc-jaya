import React, { useEffect } from 'react'
import { getToken } from './Utils'
import Layout from './Layout/Layout'
import { Switch, Route, Redirect } from 'react-router-dom'
import Dashboard from './Dashboard/Dashboard'
import ListSupplier from './Supplier/ListPage'
import CreateSupplier from './Supplier/CreatePage'
import EditSupplier from './Supplier/EditPage'

function ProtectedPage(props) {
  const { history } = props
  useEffect(() => {
    const token = getToken()
    if (!token) {
      history.push('/login')
    }
  })

  return (
    <Layout {...props}>
      <Switch>
        <Route path="/suppliers/create" component={CreateSupplier} />
        <Route path="/suppliers/:supplierId" component={EditSupplier} />
        <Route path="/suppliers" component={ListSupplier} />
        <Route exact path="/" component={Dashboard} />
        <Redirect to="/" />
      </Switch>
    </Layout>
  )
}

export default ProtectedPage
